import multiprocessing
import subprocess
import sys
import time
import json
from PySide6.QtWidgets import QApplication, QMainWindow, QAbstractItemView, QHeaderView, QTableWidgetItem
from PySide6.QtCore import QFile
from PySide6.QtGui import QTextCursor
from ui_mainwindow import Ui_MainWindow

# {
#    "Command":"get",
#    "Output":{
#       "0xA19E218FA002E":{
#          "activationState":"Activated",
#          "name":"iPhone",
#          "serialNumber":"F17VKA5PJCLJ"
#       },
#       "0x1C30303AB8801E":{
#          "activationState":"Activated",
#          "name":"iPhone",
#          "serialNumber":"NX4F9133FM"
#       },
#       "Errors":{
#          "0x1C30303AB8801E":{
            
#          },
#          "0xA19E218FA002E":{
            
#          }
#       }
#    },
#    "Type":"CommandOutput",
#    "Devices":[
#       "0xA19E218FA002E",
#       "0x1C30303AB8801E"
#    ]
# }

class Device:
    def __init__(self, ecid, name, activationState, serialNumber, errors):
        self.ecid = ecid
        self.name = name
        self.activationState = activationState
        self.serialNumber = serialNumber
        self.errors = errors

    def __eq__(self, other): 
        if not isinstance(other, Device):
            # don't attempt to compare against unrelated types
            return False
            
        return self.ecid == other.ecid

    def __hash__(self):
        # necessary for instances to behave sanely in dicts and sets.
        return hash((self.ecid))

    @staticmethod
    def from_json_to_list(json_str):
        json_dct = json.loads(json_str)
        device_list = []
        for device_ecid in json_dct['Devices']:
            temp_device = Device(device_ecid, json_dct['Output'][device_ecid]['name'], json_dct['Output'][device_ecid]['activationState'], json_dct['Output'][device_ecid]['serialNumber'], json_dct['Output']['Errors'][device_ecid])
            device_list.append(temp_device)
        return device_list

    @staticmethod
    def get_devices():
        try:
            # dd=subprocess.run(' /Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/cfgutil get activationState name serialNumber'
            #            , shell=True,capture_output=True, text=True )
            devices_get = subprocess.run(['cfgutil', 'get' , 'activationState', 'name' , 'serialNumber', '-f', '--format' , 'JSON'] 
            ,cwd='/Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/',capture_output=True, text=True )
            return devices_get
        except Exception as ex:
            return 'Get Devices - ' + ex.strerror
        

    @staticmethod
    def activate_device(device):

        output = []
        try:
            temp_activate_output = subprocess.run(['cfgutil', 'activate', '--ecid', device.ecid ]
                        , cwd='/Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/', capture_output=True, text=True)
            output.append(temp_activate_output)

            temp_prepare_output = subprocess.run(['cfgutil', 'prepare',  '--skip-all', '--ecid', device.ecid]
                            , cwd='/Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/', capture_output=True, text=True)
            output.append(temp_prepare_output)

            temp_install_output = subprocess.run(['cfgutil', 'install-app', '/Users/ondrej_zak/Desktop/BAC/iQT-3.1.2-4673/iCutie-SkyEcho-3.1.2-4673.ipa', '--ecid', device.ecid ]
                            , cwd='/Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/', capture_output=True, text=True)
            output.append(temp_install_output)

            # output.append(subprocess.run([' /Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/cfgutil', 'activate', '--ecid', device.ecid ]
            #             , shell=True,capture_output=True, text=True))
            # output.append(subprocess.run([' /Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/cfgutil', 'prepare',  '--skip-all', '--ecid', device.ecid]
            #             , shell=True,capture_output=True, text=True))
            # output.append(subprocess.run([' /Applications/Apple\ Configurator\ 2.15.app/Contents/MacOS/cfgutil', 'install-app', '/Users/ondrej_zak/Desktop/BAC/iQT-3.1.2-4673/iCutie-SkyEcho-3.1.2-4673.ipa', '--ecid', device.ecid ]
            #             , shell=True,capture_output=True, text=True))
        except Exception as ex:
            error_string = f'{ex.strerror} - {ex.filename}'
            output.append(error_string)
        
        dict = {'output': output, 'device': device}
        return dict

    @staticmethod
    def check_devices(connected_devices):
        for con_device in connected_devices:
            if con_device in loaded_devices:
                for load_device in loaded_devices:
                    if con_device == load_device:
                        if con_device.activationState != load_device.activationState and con_device.activationState == 'Activated':
                            load_device.activationState = con_device.activationState
                        else:
                            pass
            else:
                loaded_devices.add(con_device)
                if con_device.activationState == 'Unactivated':
                    to_activate_devices.add(con_device)
        
        #TEST DONT KNOW IF WORKS
        temp_delete = []
        for load_device in loaded_devices:
            if load_device not in connected_devices:
                temp_delete.append(load_device)
        
        for x in temp_delete:
            loaded_devices.remove(x)




loaded_devices = set()

activating_devices = set()
to_activate_devices = set()




class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.start_pushButton.clicked.connect(self.start_pushButton_click)
        self.ui.activate_pushButton.clicked.connect(self.activate_pushButton_click)
        self.ui.stop_pushButton.clicked.connect(self.stop_pushButton_click)
        self.ui.devices_tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ui.devices_tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ui.log_plainTextEdit.setReadOnly(True)
        self.ui.log_plainTextEdit.setUndoRedoEnabled(False)
        self.numberOfItemsInTable = 0
        self.pool = multiprocessing.Pool(cpu_count)

    def start_pushButton_click(self):
        
        dd = Device.get_devices()
        if type(dd) == str:
            self.ui.log_plainTextEdit.appendPlainText(dd)
        elif dd.returncode == 0:
            connected_devices = Device.from_json_to_list(dd.stdout)
            Device.check_devices(connected_devices)
        else:
            self.ui.log_plainTextEdit.appendPlainText(dd.stderr)

        # #DEBUG
        # json_str = '{"Command":"get","Output":{"0xA19E218FA002E":{"activationState":"Activated","name":"iPhone","serialNumber":"F17VKA5PJCLJ"},"0x1C30303AB8801E":{"activationState":"Unactivated","name":"iPhone","serialNumber":"NX4F9133FM"},"Errors":{"0x1C30303AB8801E":{},"0xA19E218FA002E":{}}},"Type":"CommandOutput","Devices":["0xA19E218FA002E","0x1C30303AB8801E"]}'
        # connected_devices = Device.from_json_to_list(json_str)
        # self.ui.log_plainTextEdit.appendPlainText(json_str)
        # Device.check_devices(connected_devices)
                    
        self.refresh_devices_in_table()

    def add_device_to_table(self, device):
        self.ui.devices_tableWidget.insertRow(self.numberOfItemsInTable)
        self.ui.devices_tableWidget.setItem(self.numberOfItemsInTable, 0, QTableWidgetItem(device.name))
        self.ui.devices_tableWidget.setItem(self.numberOfItemsInTable, 1, QTableWidgetItem(device.serialNumber))
        self.ui.devices_tableWidget.setItem(self.numberOfItemsInTable, 2, QTableWidgetItem(device.ecid))
        self.ui.devices_tableWidget.setItem(self.numberOfItemsInTable, 3, QTableWidgetItem(device.activationState))
        self.ui.devices_tableWidget.setItem(self.numberOfItemsInTable, 4, QTableWidgetItem(str(len(device.errors))))
        self.numberOfItemsInTable+=1
    
    def refresh_devices_in_table(self):
        self.ui.devices_tableWidget.setRowCount(0)
        self.numberOfItemsInTable = 0
        for device in loaded_devices:
            self.add_device_to_table(device)
                
    def activate_pushButton_click(self):
        for device in to_activate_devices:
            if device not in activating_devices:
                activating_devices.add(device)
                self.pool.apply_async(Device.activate_device, args=(device,), callback=self.add_stdout_into_log, error_callback=self.add_stderr_into_log)

    def add_stdout_into_log(self, output):
        for o in output['output']:
            if type(o) == str:
                self.ui.log_plainTextEdit.appendPlainText(o)
                activating_devices.remove(output['device'])
                # to_activate_devices.add(output['device'])
            elif o.returncode == 0:
                self.ui.log_plainTextEdit.appendPlainText(o.stdout)
                activating_devices.remove(output['device'])
            else:
                self.ui.log_plainTextEdit.appendPlainText(o.stderr)
                activating_devices.remove(output['device'])

        self.ui.log_plainTextEdit.verticalScrollBar().setValue(self.ui.log_plainTextEdit.verticalScrollBar().maximum())

    def add_stderr_into_log(self, output):
        for o in output['output']:
            if type(o) == str:
                self.ui.log_plainTextEdit.appendPlainText(o)
                activating_devices.remove(output['device'])
                # to_activate_devices.add(output['device'])
            elif o.returncode == 0:
                self.ui.log_plainTextEdit.appendPlainText(o.stdout)
                activating_devices.remove(output['device'])
            else:
                self.ui.log_plainTextEdit.appendPlainText(o.stderr)
                activating_devices.remove(output['device'])

        self.ui.log_plainTextEdit.verticalScrollBar().setValue(self.ui.log_plainTextEdit.verticalScrollBar().maximum())

    def stop_pushButton_click(self):
        self.pool.close()
        self.pool.join()

    def force_stop_pushButton_click(self):
        self.pool.terminate()
        self.pool.join()


if __name__ == "__main__":
    cpu_count = multiprocessing.cpu_count() -1
    if cpu_count <= 1:
        cpu_count = 2
   
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec())