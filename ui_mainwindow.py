# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 6.2.4
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractScrollArea, QApplication, QHBoxLayout, QHeaderView,
    QMainWindow, QMenuBar, QPlainTextEdit, QPushButton,
    QSizePolicy, QStatusBar, QTableWidget, QTableWidgetItem,
    QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 800)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self._2 = QHBoxLayout()
        self._2.setObjectName(u"_2")
        self.devices_tableWidget = QTableWidget(self.centralwidget)
        if (self.devices_tableWidget.columnCount() < 5):
            self.devices_tableWidget.setColumnCount(5)
        __qtablewidgetitem = QTableWidgetItem()
        self.devices_tableWidget.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.devices_tableWidget.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.devices_tableWidget.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.devices_tableWidget.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.devices_tableWidget.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        self.devices_tableWidget.setObjectName(u"devices_tableWidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.devices_tableWidget.sizePolicy().hasHeightForWidth())
        self.devices_tableWidget.setSizePolicy(sizePolicy)
        self.devices_tableWidget.setBaseSize(QSize(0, 0))
        self.devices_tableWidget.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.devices_tableWidget.setShowGrid(True)
        self.devices_tableWidget.setSortingEnabled(True)
        self.devices_tableWidget.setColumnCount(5)
        self.devices_tableWidget.horizontalHeader().setVisible(True)
        self.devices_tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.devices_tableWidget.horizontalHeader().setMinimumSectionSize(57)
        self.devices_tableWidget.horizontalHeader().setProperty("showSortIndicator", True)
        self.devices_tableWidget.horizontalHeader().setStretchLastSection(False)
        self.devices_tableWidget.verticalHeader().setCascadingSectionResizes(False)
        self.devices_tableWidget.verticalHeader().setStretchLastSection(False)

        self._2.addWidget(self.devices_tableWidget)


        self.verticalLayout.addLayout(self._2)

        self.log_plainTextEdit = QPlainTextEdit(self.centralwidget)
        self.log_plainTextEdit.setObjectName(u"log_plainTextEdit")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.log_plainTextEdit.sizePolicy().hasHeightForWidth())
        self.log_plainTextEdit.setSizePolicy(sizePolicy1)
        self.log_plainTextEdit.setMinimumSize(QSize(0, 220))

        self.verticalLayout.addWidget(self.log_plainTextEdit)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.start_pushButton = QPushButton(self.centralwidget)
        self.start_pushButton.setObjectName(u"start_pushButton")

        self.horizontalLayout.addWidget(self.start_pushButton)

        self.activate_pushButton = QPushButton(self.centralwidget)
        self.activate_pushButton.setObjectName(u"activate_pushButton")

        self.horizontalLayout.addWidget(self.activate_pushButton)

        self.stop_pushButton = QPushButton(self.centralwidget)
        self.stop_pushButton.setObjectName(u"stop_pushButton")

        self.horizontalLayout.addWidget(self.stop_pushButton)


        self.verticalLayout.addLayout(self.horizontalLayout)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 22))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        # self.start_pushButton.clicked.connect(self.start_pushButton.click)
        # self.activate_pushButton.clicked.connect(self.activate_pushButton.click)
        # self.stop_pushButton.clicked.connect(self.stop_pushButton.click)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Better Apple Configurator", None))
        ___qtablewidgetitem = self.devices_tableWidget.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"Name", None));
        ___qtablewidgetitem1 = self.devices_tableWidget.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"Serial Number", None));
        ___qtablewidgetitem2 = self.devices_tableWidget.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"ECID", None));
        ___qtablewidgetitem3 = self.devices_tableWidget.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"Activation State", None));
        ___qtablewidgetitem4 = self.devices_tableWidget.horizontalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("MainWindow", u"Errors", None));
        self.start_pushButton.setText(QCoreApplication.translate("MainWindow", u"Start", None))
        self.activate_pushButton.setText(QCoreApplication.translate("MainWindow", u"Activate", None))
        self.stop_pushButton.setText(QCoreApplication.translate("MainWindow", u"Stop", None))
    # retranslateUi

